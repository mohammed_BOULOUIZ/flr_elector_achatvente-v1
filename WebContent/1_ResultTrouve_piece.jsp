<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js">
	
</script>

<title>Liste des Modeles</title>
</head>
<body>
	<!-- Card Client -->
	<div>
		<%@ include file="index.jsp"%>
	</div>

	<!-- Alerte recherche de piece par ref -->
	<div class="container">
		<c:if test="${beanPiece_ref.status=='SUCCES'}">
			<div class="alert alert-success alert-dismissible fade show mt-3"
				role="alert">
				<strong>** Succes **</strong> ** Succes **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
		<c:if test="${beanPiece_ref.status=='ERREUR'}">
			<div class="alert alert-danger alert-dismissible fade show mt-3"
				role="alert">
				<strong>** ERREUR **</strong> ** ERREUR **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
	</div>

	<div class="container">
		<table class="table table-sm mt-3 ">

			<!-- ******************	tete du tableau ****************** -->
			<thead>
				<tr>
					<th scope="col">N</th>
					<th scope="col">Reference</th>
					<th scope="col">Reference originale</th>
					<th scope="col">Nom de la piece</th>
					<th scope="col">prix TTC</th>
				</tr>
			</thead>
			<!-- ******************	corp du tableau ****************** -->
			<tbody>
				<tr>
					
					<td>
					
					</td>
					<td>${requestScope.beanPiece_ref.ref_piece}</td>
					<td>${requestScope.beanPiece_ref.ref_piece_originale}</td>
					<td>${requestScope.beanPiece_ref.designation}</td>
					<td>${(requestScope.beanPiece_ref.pvht*.19)+requestScope.beanPiece_ref.pvht}</td>
					
				

					<!-- ******************	Liste des pieces ****************** -->

						<td>
							<form name="achter" action="Main" method="post">
								<button type="submit" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
							</form>
						</td>
					
					</tr>
			</tbody>
		</table>

	</div>


	

</body>
</html>