<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Liste des Modeles</title>
</head>
<body>
	<!-- Card Client -->
	<div>
		<%@ include file="index.jsp"%>
	</div>
	<div class="container">

		<table class="table table-sm mt-3 ">

			<!-- ******************	tete du tableau ****************** -->
			<thead>
				<tr>
					<th scope="col">N</th>
					<th scope="col">Reference</th>
					<th scope="col">Reference originale</th>
					<th scope="col">Nom de la piece</th>
					<th scope="col">prix TTC</th>
				</tr>
			</thead>
			<!-- ******************	corp du tableau ****************** -->
			<tbody>
				<c:forEach var="PiecedsBeancas" items="${lebean_lspccas.lst}"
					varStatus="numloop">
					<tr>
						<th scope="row">${numloop.index}</th>
						<td><c:out value="${PiecedsBeancas.ref_piece}" /></td>
						<td><c:out value="${PiecedsBeancas.ref_piece_originale}" /></td>
						<td><c:out value="${PiecedsBeancas.designiation}" /></td>
						<td><c:out value="${(PiecedsBeancas.pvht*0.19)+PiecedsBeancas.pvht}"/></td>

						<!-- ******************	Liste des pieces ****************** -->

						<td>
							<form name="achter" action="Main" method="post">
								<button type="submit" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>



</body>
</html>