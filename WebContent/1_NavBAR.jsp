<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>navbar</title>
</head>
<body>

	<!-- Navbare -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary"> <a
		class="navbar-brand" href="index.jsp">ELECTRODiscunt</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<c:choose>
			<c:when test="${beanParam.statut=='SUCCES'}">
				<ul class="navbar-nav mr-auto">
					<!-- Accueil -->
					<li class="nav-item dropdown active"><a class="nav-link "
						href="index.jsp" title="home" aria-hidden="true"> <i
							class="fas fa-home"></i> Accueil
					</a></li>
					<!-- Gerer Type -->
					<li class="nav-item dropdown active"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Type </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item active" href="Main?action=listerTypes"><i
								class="fas fa-play-circle"></i> Afficher</a> <a
								class="dropdown-item active" href="Main?action=ajouterType"><i
								class="fas fa-plus-circle"></i> Ajouter</a>
						</div></li>
					<!-- Gerer Marque -->
					<li class="nav-item dropdown active"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Marque </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item active" href="Main?action=listerMarques"><i
								class="fas fa-play-circle"></i> Afficher</a> <a
								class="dropdown-item active" href="Main?action=ajouterMarque"><i
								class="fas fa-plus-circle"></i> Ajouter</a>
						</div></li>
					<!-- Gerer Modele -->
					<li class="nav-item dropdown active"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Modele </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item active" href="Main?action=listerModeles"><i
								class="fas fa-play-circle"></i> Afficher</a> <a
								class="dropdown-item active" href="Main?action=ajouterModele"><i
								class="fas fa-plus-circle"></i> Ajouter</a>
						</div></li>

					<!-- Gerer Piece -->
					<li class="nav-item dropdown active"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Piece </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item active" href="Main?action=listerPieces"><i
								class="fas fa-play-circle"></i> Afficher</a> <a
								class="dropdown-item active" href="Main?action=ajouterPiece"><i
								class="fas fa-plus-circle"></i> Ajouter</a>
						</div></li>
					<!-- Gerer Categorie -->
					<li class="nav-item dropdown active"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Categorie </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item active"
								href="Main?action=listerCategories"><i
								class="fas fa-play-circle"></i> Afficher</a> <a
								class="dropdown-item active" href="Main?action=ajouterCategorie"><i
								class="fas fa-plus-circle"></i> Ajouter</a>
						</div></li>
					<!-- Associer une piece � un modele -->
					<li class="nav-item active"><a class="nav-link"
						href="Main?action=associerPM">Associer Piece-Modele <span
							class="sr-only">(current)</span></a></li>
					<li>
						<form name="login" action="Main" method="post">
						<p class="font-weight-bold">Monsieur "${beanParam.prenom}"</p>
						<button class="btn btn-primary " type="submit">
						<i class="fas fa-sign-out-alt"></i></button>
						<input type="hidden" name="action" value="deconnecte"></input>
						</form>
					</li>
				</ul>
			</c:when>
			<c:when test="${beanParam.statut==null}">
				<!-- Login -->
				<form name="login" action="Main" method="post">
					<input class="form-control mr-sm-2" type="text"
						placeholder="identifiant..." aria-label="Search" name="id"> 
					<input
						class="form-control mr-sm-2" type="password"
						placeholder="mot de passe" aria-label="Search" name="pswd">
					<input type="hidden" name="action" value="connecte"></input>
					<button class="btn btn-primary " type="submit">Login</button>
				</form>
			</c:when>
		</c:choose>
	</div>
	</nav>



</body>
</html>