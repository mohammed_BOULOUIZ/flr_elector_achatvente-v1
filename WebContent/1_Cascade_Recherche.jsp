<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Accueil</title>
</head>
<body>

	<!-- ********************************Recherche en cascade********************************************** -->
	<div class="container">
		<div class="row">
			<div class="col">

				<div class="card text-white bg-secondary mt-3 ml-auto"
					style="max-width: 50rem;">
					<div class="card-header">Lister toutes les pieces</div>
					<div class="card-body">
						<!-- ********************************afficher un modele********************************************** -->
						<div class="container">
							<form name="Cascade_modele" action="Main" method="post">
								<div class="form-row align-items-center">
									<div class="col-auto my-auto">
										<label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
										<select class="custom-select mr-sm-2"
											id="inlineFormCustomSelect" name="id_type">
											<option>selectionnez un Type</option>
											<c:forEach var="lebean_listy" items="${lebean_lsty.lst}"
												varStatus="numloop">
												<option value="${lebean_listy.id_type}">${lebean_listy.libelle_type}</option>
											</c:forEach>
										</select>
									</div>

									<div class="col-auto my-auto">
										<select class="custom-select mr-sm-2 ml-2"
											id="inlineFormCustomSelect" name="id_marque">
											<option>selectionnez une Marque</option>
											<c:forEach var="lebean_lismq" items="${lebean_lsmq.lst}"
												varStatus="numloop">
												<option value="${lebean_lismq.id_marque}">${lebean_lismq.libelle_marque}</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-auto my-4">
										<button type="submit" class="btn btn-primary">Lister
											les Modeles</button>
										<input type="hidden" name="action" value="cascademodele">
									</div>
								</div>
							</form>
						</div>
						<!-- ********************************afficher les pieces lors de la selection du modele********************************************** -->
						<div class="container">
							<form name="Cascade_piece" action="Main" method="post">
								<div class="form-row align-items-center">
									<div class="col-auto my-auto">
										<label class="mr-sm-2 sr-only" for="inlineFormCustomSelect"></label>
										<select class="custom-select mr-sm-2"
											id="inlineFormCustomSelect" name="id_categorie">
											<option>selectionnez une Categorie</option>
											<c:forEach var="lebean_liscat" items="${lebean_lscat.lst}"
												varStatus="numloop">
												<option value="${lebean_liscat.id_categorie}">${lebean_liscat.libelle_categorie}</option>
											</c:forEach>
										</select>
									</div>

									<div class="col-auto my-auto">
										<select class="custom-select mr-sm-2 mr-0 mt-4"
											id="inlineFormCustomSelect" name="ref_modele">
											<option>selectionnez un Modele</option>
											<c:forEach var="lebean_lismocas" items="${lebean_lsmocas.lst}"
												varStatus="numloop">
												<option value="${lebean_lismocas.ref_modele}">${lebean_lismocas.ref_modele}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-auto my-4">
										<button type="submit" class="btn btn-primary">Afficher les Pieces</button>
										<input type="hidden" name="action" value="listerpiececascade">
									</div>
							</form>
						</div>
					</div>
				</div>

			</div>
			<!-- ********************************Recherche par reference de piece********************************************** -->
			<div class="col">
				<div class="container ">
					<form name="piecebyref" action="Main" method="post">
						<div class="card text-white bg-info mt-3 mr-auto"
							style="max-width: 50rem;">
							<div class="card-header">Chercher une piece</div>
							<div class="card-body">
								<!-- ********************************Zone de text "inserer la ref********************************************** -->

								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="inputGroup-sizing-default">R�f�rence</span>
									</div>
									<input type="text" class="form-control"
										aria-label="Sizing example input"
										aria-describedby="inputGroup-sizing-default"
										placeholder="BSCG10203050..." name="refrechercher">
									<button type="submit" class="btn btn-primary ml-2">Chercher</button>
									<input type="hidden" name="action" value="piecebyref">
									
								</div>

								<!-- ************************************************************************************************************** -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


</body>
</html>