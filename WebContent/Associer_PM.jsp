<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js">
</script>
<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
		crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<title>Ajouter Modele</title>
</head>
<body>

<!-- la NavBAR -->
<div>
		<%@ include file="1_NavBAR.jsp"%>
</div>
<!-- Alerte associer -->
	<div class="container">
		<c:if test="${lebean_ass.status=='SUCCES'}">
			<div class="alert alert-success alert-dismissible fade show mt-3"
				role="alert">
				<strong>** Succes **</strong> ** Succes **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
		<c:if test="${lebean_ass.status=='ERREUR'}">
			<div class="alert alert-danger alert-dismissible fade show mt-3"
				role="alert">
				<strong>** ERREUR **</strong> ** ERREUR **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
	</div>
	
	
<div class="container">

<div class="card mt-3">
<div class="card-body">

<form name="associerPM" action="Main" method="post">	
 	<div class="row">
		<div class="col">
    		<div class="form-group">
			    <select class="form-control" id="exampleFormControlSelect1" name="ref_piece" required>
			    	<option>Selectionnez une Piece</option>
			    	  <c:forEach var="lebean_lstpc" items="${lebean_lspc.lst}" varStatus="numloop" >
			      	<option value="${lebean_lstpc.ref_piece}" name="ref_piece">${lebean_lstpc.designiation}</option>	<!-- dans option ref_piece / affichage : ref + designation -->	
			      </c:forEach>
			    </select>
			</div>
		</div>  
		
    	<div class="col">
		    <div class="form-group">
				<select class="form-control" id="exampleFormControlSelect1" name="ref_modele" required>
					<option>Selectionnez un Modele</option>
			      		<c:forEach var="lebean_lsmo" items="${lebean_lsmoass.lst}" varStatus="numloop">
			      	<option value="${lebean_lsmo.ref_modele}" name="ref_modele">${lebean_lsmo.ref_modele}</option>
				</c:forEach>
				</select>
		    </div>
		</div>
	</div>

<!-- bouton associer -->

		<input type="hidden" name="action" value="associerPM"></input>
		<button type="submit" class="btn btn-primary">Associer</button>
</form>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" 
crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous">
</script>
</body>
</html>