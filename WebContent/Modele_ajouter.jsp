<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js">
	
</script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<title>Ajouter Modele</title>
</head>
<body>

	<!-- la NavBAR -->
	<div>
		<%@ include file="1_NavBAR.jsp"%>
	</div>
	
	<!-- Alerte ajouter -->
	<div class="container">
		<c:if test="${lebeanajMo.status=='SUCCES'}">
			<div class="alert alert-success alert-dismissible fade show mt-3"
				role="alert">
				<strong>** Succes **</strong> ** Succes **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
		<c:if test="${lebeanajMo.status=='ERREUR'}">
			<div class="alert alert-danger alert-dismissible fade show mt-3"
				role="alert">
				<strong>** ERREUR **</strong> ** ERREUR **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
	</div>


	<div class="container">
		<div class="card">
			<div class="card-body">
				<form name="ajouterModele" action="Main" method="post">
					<div class="row">
						<div class="col">
							<div class="form-group">
								<select class="form-control" id="exampleFormControlSelect1"
									name="id_type" required>
									<c:forEach var="lebean_listy" items="${lebean_lsty.lst}"
										varStatus="numloop">
										<option value="${lebean_listy.id_type}" name="id_type">${lebean_listy.libelle_type}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="col">
							<div class="form-group">
								<select class="form-control" id="exampleFormControlSelect1"
									name="id_marque" required>
									<c:forEach var="lebean_lismq" items="${lebean_lsmq.lst}"
										varStatus="numloop">
										<option value="${lebean_lismq.id_marque}" name="id_marque">${lebean_lismq.libelle_marque}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>

					<!-- Ajouter un Modele -->

					<input type="hidden" name="action" value="ajouterModele"></input> <label
						for="formGroupExampleInput">Saisissez le nom du Modele</label> <input
						type="text" name="libelleINS" class="form-control"
						id="formGroupExampleInput"
						placeholder="SMS25AW00F, SMV41D00EU, ..." required>
					<button type="submit" class="btn btn-primary">Ajouter un
						Modele</button>
				</form>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous">
		
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous">
		
	</script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous">
</script>
</body>
</html>