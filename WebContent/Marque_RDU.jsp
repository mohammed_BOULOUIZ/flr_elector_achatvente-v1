<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
		crossorigin="anonymous">
<!-- icone -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<title>Lister Marque</title>
</head>
<body>

	<!-- la NavBAR -->
<div>
		<%@ include file="1_NavBAR.jsp"%>
</div>
<!-- Alerte suppression -->
	<div class="container">
		<c:if test="${lebean_supp.status=='SUCCES'}">
			<div class="alert alert-success alert-dismissible fade show mt-3"
				role="alert">
				<strong>** Succes **</strong> ** Succes **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
		<c:if test="${lebean_supp.status=='ERREUR'}">
			<div class="alert alert-danger alert-dismissible fade show mt-3"
				role="alert">
				<strong>** ERREUR **</strong> ** ERREUR **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
	</div>
	
	<!-- Alerte suppression -->
	<div class="container">
		<c:if test="${lebean_up.status=='SUCCES'}">
			<div class="alert alert-success alert-dismissible fade show mt-3"
				role="alert">
				<strong>** Succes **</strong> ** Succes **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
		<c:if test="${lebean_up.status=='ERREUR'}">
			<div class="alert alert-danger alert-dismissible fade show mt-3"
				role="alert">
				<strong>** ERREUR **</strong> ** ERREUR **
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>
	</div>
<div class="container">

<table class="table table-sm">

			<!-- ******************	tete du tableau ****************** -->							
<thead>
				<tr>
					<th scope="col">N</th>
					<th scope="col">Marque</th>
					<th scope="col">Supprimer</th>
					<th scope="col">Modifier</th>
				</tr>
</thead>
			<!-- ******************	corp du tableau ****************** -->		
<tbody>
<c:forEach var="marquedsBean" items="${lebean_lsmq.lst}" varStatus="numloop" >	
<tr>
     		<th scope="row">${numloop.index}</th>
      		<td><c:out value="${marquedsBean.libelle_marque}"/></td>
	      		
			<!-- ******************	Supprimer ****************** -->				
	      		
			<td>	
				<form name="supprimer" action="Main" method="post">
	 				<input type="hidden" name="libelleSUP" value="${marquedsBean.libelle_marque}"></input>
	 				<input type="hidden" name="action" value="supprimerMarque"></input> 
	 				<button type="submit" class="btn btn-primary"><i class="fas fa-minus-circle"></i></button>
	 			</form>			
			</td>
			
	
			<!-- ******************	Modifier ****************** -->				
			<td>
				<form name="modifier_modal" action="Main" method="post">
						
				
					<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal${numloop.index}"><i class="fas fa-pen-square"></i></button>

			<!-- Modal -->
				<div class="modal fade" id="exampleModal${numloop.index}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
							      <div class="modal-body">
							        <input type="hidden" name="action" value="modifierMarque"></input>
									<label for="formGroupExampleInput">Saisissez le nom du Type</label>
									<input type="text" name="new_libelle" class="form-control" id="formGroupExampleInput" placeholder="nouveau nom..." required="">
							      </div>
							      
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <button type="submit" class="btn btn-primary">Modifier</button>
							        <input type="hidden" name="libelleUP" value="${marquedsBean.libelle_marque}"></input>
							     </div>
							        
							       
						</div>
					</div>
				</div>
			  </form>	
			</td>
	
		
</tr>
</c:forEach>		
</tbody>
</table>
     
</div>











<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" 
crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous">
</script>
</body>
</html>