package domain;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("30304b68-818a-4c97-9412-05bb259a700e")
public class Document {
	
	/* Attributs */
    @objid ("1b8e0234-51a9-43a4-80ff-194b77a95b66")
    private int id_document;
    @objid ("41f831d0-bf58-4f6a-a60c-b521f797d00c")
    private String url_document;
    @objid ("3b1fa3b9-c7d5-45b1-a55d-587b1cc1eab6")    
    private String type_document;
    private String ref_piece;
	
    /* Constructeur */
    public Document(int id_document, String ref_piece, String url_document, String type_document) {
		super();
		this.id_document = id_document;
		this.url_document = url_document;
		this.type_document = type_document;
		this.ref_piece = ref_piece;
	}

    public Document(String ref_piece, String url_document, String type_document) {
		super();
		this.url_document = url_document;
		this.type_document = type_document;
		this.ref_piece = ref_piece;
	}



	/* Get_Set (!! Set Id_document Interdit !!)*/
	public int getId_document() {
		return id_document;
	}

	public void setId_document(int id_document) {
		this.id_document = id_document;
	}

	public String getUrl_document() {
		return url_document;
	}

	public void setUrl_document(String url_document) {
		this.url_document = url_document;
	}

	public String getType_document() {
		return type_document;
	}

	public void setType_document(String type_document) {
		this.type_document = type_document;
	}

	public String getRef_piece() {
		return ref_piece;
	}





    
    
    
}
