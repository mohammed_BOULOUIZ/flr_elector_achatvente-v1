package domain;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class BDDConnexion {
	private static Connection cnx;

	public static Connection getConnexion() {

		try {
			Context initialCtxt = new InitialContext();
			
			DataSource dataSource = (DataSource) initialCtxt.lookup("java:comp/env/jdbc/maDS");
			cnx = dataSource.getConnection();
		} catch (NamingException ne) {
			System.out.println(ne.getMessage() + "et donc" + ne.getCause());
			System.exit(-1);
		} catch (Exception e) {
			System.out.println(e.getMessage() + "et donc" + e.getCause());
			System.exit(-1);
		}

		return cnx;
	}

	public static void close() throws Exception {

		try {
			if (cnx != null) {
				cnx.close();
				cnx = null;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}

}