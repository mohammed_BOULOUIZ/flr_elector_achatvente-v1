package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Type;

public interface TypeDAO {
	
	public abstract Type insert (Type ty) throws BDDException;
	public abstract ArrayList<Type> getAll() throws BDDException;
	public abstract int update (Type ty, String new_lib) throws BDDException;
	public abstract int delete (Type ty) throws BDDException;

}
