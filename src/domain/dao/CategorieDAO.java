package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Categorie;

public interface CategorieDAO {

	public abstract Categorie insert (Categorie ca) throws BDDException;
	public abstract ArrayList<Categorie> getAll() throws BDDException;
	public abstract int update (Categorie ca, String new_lib) throws BDDException;
	public abstract int delete (Categorie ca) throws BDDException;
}
