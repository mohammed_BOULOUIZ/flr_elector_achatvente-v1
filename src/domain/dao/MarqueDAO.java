package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Marque;

public interface MarqueDAO {
	
	public abstract ArrayList<Marque> getAll() throws BDDException;
	public abstract Marque insert (Marque marque) throws BDDException;
	public abstract int update (Marque marque, String new_lib) throws BDDException;
	public abstract int delete (Marque marque) throws BDDException;

}
