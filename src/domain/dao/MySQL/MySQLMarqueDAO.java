package domain.dao.MySQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Marque;
import domain.Type;
import domain.dao.MarqueDAO;

public class MySQLMarqueDAO implements MarqueDAO {
	/**
	 * @param cnx
	 * @param libelle_marque
	 * @param mStmt;
	 * @param rs;
	 * @param almarque;
	 * @param m;
	 * @param idm;
	 * @param libm;
	 *            lmarque=new ArrayList<Marque>();
	 * 
	 * @return Get_Set des id et du libelle de la calsse Marque
	 * 
	 * 
	 * 
	 * @author boulouiz
	 *
	 */
	private Connection cnx;

	public MySQLMarqueDAO(Connection cnx) {
		super();
		this.cnx = cnx;
	}

	public MySQLMarqueDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public ArrayList<Marque> getAll() throws BDDException {

		/* Declaration */
		CallableStatement cStmt;
		ResultSet rs;
		ArrayList<Marque> almarque;
		Marque mrq;
		int idm;
		int retour;
		int FOUNDROWS;
		String libm;
		almarque = new ArrayList<Marque>();

		try {
			// 1- cr�e la variable qui doit ex�cuter la requette
			cStmt = cnx.prepareCall("CALL ps_select_MARQUE (?,?)");

			// 2- donne valeur du parametre
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);

			// 3- ex�cute requete
			rs = cStmt.executeQuery();
			while (rs.next()) {
				idm = rs.getInt("ID_MARQUE");
				libm = rs.getString("LIBELLE_MARQUE");
				mrq = new Marque(idm, libm);
				almarque.add(mrq);
			}
			retour = cStmt.getInt(1);
			FOUNDROWS = cStmt.getInt(2);
			// 4- fin de l'execution
			cStmt.close();

			// 5- catch les exceptions
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		// 6- retourner l'ArrayList
		return almarque;
	}

	public Marque insert(Marque marque) throws BDDException {
		/* Declaration */
		CallableStatement cStmt;
		Marque mrq = null;
		int id;
		int retour;

		/* 1- cree la variable qui doit executer la requette ou la PS */

		try {
			cStmt = cnx.prepareCall("CALL ps_insert_MARQUE (?, ?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, marque.getLibelle_marque());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.executeQuery();

			/* 4- recuperer les donnees de sortie */
			id = cStmt.getInt(4);
			retour = cStmt.getInt(2);
			mrq = new Marque(id,
					marque.getLibelle_marque()); /* le return pour l'etape 7 */

			/* 5- fin de l'execution */
			cStmt.close();
			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());

		}

		/* 7- retourner (l'ArrayList, id...) */
		return mrq;
	}

	public int update(Marque marque, String new_lib) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_update_MARQUE (?,?,?,?);");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, marque.getLibelle_marque());
			cStmt.setString(2, new_lib);

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;
	}

	public int delete(Marque marque) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_MARQUE(?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, marque.getLibelle_marque());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/*
		 * 7- retourner la valeur de "retour" qui sera utiliser � peupler le
		 * statut du bean
		 */
		return retour;
	}

}
