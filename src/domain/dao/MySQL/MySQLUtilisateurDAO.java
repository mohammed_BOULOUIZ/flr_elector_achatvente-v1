package domain.dao.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Parametre;

public class MySQLUtilisateurDAO {

	private Connection cnx;
	

	public MySQLUtilisateurDAO(Connection cx) {
		cnx = cx;
	}

	public MySQLUtilisateurDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Parametre getUser(Parametre param) throws BDDException {
		PreparedStatement pStmt;
		ResultSet rs;
		Parametre parametre = null;
		String id;
		String pswd;
		String nom;
		String prenom;

		try {
			pStmt = cnx
					.prepareStatement("SELECT ID, PASSEWORD, NOM, PRENOM FROM PARAMETRE WHERE ID=? and PASSEWORD = ?");

			pStmt.setString(1, param.getId());
			pStmt.setString(2, param.getPasseword());

			rs = pStmt.executeQuery();

			while (rs.next()) {
				id = rs.getString("ID");
				pswd = rs.getString("PASSEWORD");
				nom = rs.getString("NOM");
				prenom = rs.getString("PRENOM");

				parametre = new Parametre(id, pswd, nom, prenom);

			}
			pStmt.close();
		} catch (SQLException e) {
			System.out.println(e.getErrorCode()+e.getMessage());
		}
		if (parametre!=null){
		return parametre;
		}else{
		return null;
		}
	}
}
