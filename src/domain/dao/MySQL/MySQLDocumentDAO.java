package domain.dao.MySQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Document;
import domain.Modele;
import domain.Piece;
import domain.dao.DocumentDAO;

public class MySQLDocumentDAO implements DocumentDAO {
	private Connection cnx;

	public MySQLDocumentDAO(Connection cx) {
		cnx = cx;
	}

	public MySQLDocumentDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Document insert(Document document, Piece piOrigine) throws BDDException {

		/* 0- Declaration de variable */
		CallableStatement cStmt;

		int rowCount = 0;
		int retour;
		int id_document;

		Document doc = null;

		try {
			/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
			cStmt = cnx.prepareCall("Call ps_associe_insert_PIECE_DOCUMENT(?, ?, ?, ?, ?)");

			/* 2- donne les valeurs des parametres d'entrer */
			/* parametres � entrer */
			cStmt.setString(1, piOrigine.getRef_piece());
			cStmt.setString(2, document.getUrl_document());
			cStmt.setString(3, document.getType_document());

			// prepare parametre out
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(5, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(6, java.sql.Types.INTEGER);

			// execute requete procedure
			cStmt.execute();

			// recuperer les parametres de sortie de la procedure

			rowCount = cStmt.getInt(2);
			retour = cStmt.getInt(3);
			id_document = cStmt.getInt(4);

			doc = new Document(id_document, piOrigine.getRef_piece(), document.getUrl_document(),
					document.getType_document());
			// fermer la requete et pas la connexion
			cStmt.close();

			if (retour == 1062) // nom duplique
				throw new BDDException(1062, "nom existant");

			if (retour == 10000) // erreur SQL dans la procedure
				throw new BDDException(10000, "erreur procedure...");
		}
		// gerer le code d'erreur SQL

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());


		}
		return doc;
	}

	@Override
	public ArrayList<Document> getBy(Piece piOrigine) throws BDDException {
		/* 0- Declaration de variable */
		CallableStatement cStmt;
		ResultSet rs;

		int id_document;
		String ref_piece;
		String url_document;
		String type_document;
		Document document;
		ArrayList<Document> aldocument;
		aldocument = new ArrayList<Document>();

		int retour = 0, ROWCOUNT = 0;

		try {
			/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
			cStmt = cnx.prepareCall("CALL ps_select_DOCUMENT(?,?)");

			/* 2- donne les valeurs des parametres */

			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			rs = cStmt.executeQuery();
			while (rs.next()) {
				id_document = rs.getInt("ID_DOCUMENT");
				ref_piece = rs.getString("REF_MODELE");
				url_document = rs.getString("URL_DOCUMENT");
				type_document = rs.getString("TYPE_DOCUMENT");
				document = new Document(id_document, ref_piece, url_document, type_document);
				aldocument.add(document);
			}
			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(retour);
			ROWCOUNT = cStmt.getInt(ROWCOUNT);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- catcher les exceptions ajouter le try en 1 */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom dupliqu�
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return aldocument;

	}

	@Override
	public int delete(Document document) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_DOCUMENT(?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setInt(1, document.getId_document());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/*
		 * 7- retourner la valeur de "retour" qui sera utiliser � peupler le
		 * statut du bean
		 */
		return retour;
	}

}
