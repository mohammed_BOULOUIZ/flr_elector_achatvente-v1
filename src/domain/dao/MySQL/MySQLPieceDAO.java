package domain.dao.MySQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Categorie;
import domain.Modele;
import domain.Piece;
import domain.Type;
import domain.dao.PieceDAO;

public class MySQLPieceDAO implements PieceDAO {
	private Connection cnx;

	public MySQLPieceDAO(Connection cx) {
		cnx = cx;
	}

	public MySQLPieceDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Piece insertpO(Piece piOrigine) throws BDDException {
		/* Declaration */
		CallableStatement cStmt;

		int retour, ROWCOUNT;
		int id = 0;

		/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_insert_PIECE (?,?,?,?,?,?,?,?,?)");

			/* 2- donne les valeurs des parametres */

			/* parametres � entrer */
			cStmt.setString(1, piOrigine.getRef_piece());
			cStmt.setInt(2, piOrigine.getCategorie().getId_categorie());
			cStmt.setString(3, piOrigine.getRef_piece_originale());
			cStmt.setString(4, piOrigine.getDesigniation());
			cStmt.setFloat(5, piOrigine.getPvht());
			cStmt.setInt(6, piOrigine.getStock());
			cStmt.setInt(7, piOrigine.getActive());

			/* parametres de sortie */
			cStmt.registerOutParameter(8, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(9, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			cStmt.execute();

			/* 4- recuperer les donn�es de sortie */
			String refPO = piOrigine.getRef_piece();
			int idPO_cat = piOrigine.getCategorie().getId_categorie();
			String refPOO = piOrigine.getRef_piece_originale();
			String designPO = piOrigine.getDesigniation();
			float pvPO = piOrigine.getPvht();
			int stockPO = piOrigine.getStock();
			int activePO = piOrigine.getActive();

			retour = cStmt.getInt(8);
			ROWCOUNT = cStmt.getInt(9);
			Categorie categorie = new Categorie(idPO_cat);
			Piece pieceOriginale = new Piece(refPO, categorie, refPOO, designPO, pvPO, stockPO, activePO);
			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- catcher les exceptions ajouter le try en 1 */
			if (retour == 0) {
				System.out.println("succes");
				return pieceOriginale;
			} // ok

			if (retour == 1062) // nom dupliqu�
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return null;
	}

	public Piece insertpE(Piece piEquivalente, Piece piOrigine) throws BDDException {

		/* Declaration */
		CallableStatement cStmt;

		int retour, ROWCOUNT;
		int id;

		/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_insert_PIECE (?,?,?,?,?,?,?,?,?,?)");

			/* 2- donne les valeurs des parametres */

			/* parametres � entrer */
			cStmt.setString(1, piEquivalente.getRef_piece());
			cStmt.setInt(2, piEquivalente.getCategorie().getId_categorie());
			cStmt.setString(3, piOrigine.getRef_piece_originale());
			cStmt.setString(4, piEquivalente.getDesigniation());
			cStmt.setFloat(5, piEquivalente.getPvht());
			cStmt.setInt(6, piEquivalente.getStock());
			cStmt.setInt(7, piEquivalente.getActive());

			/* parametres de sortie */

			cStmt.registerOutParameter(8, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(9, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			cStmt.execute();

			/* 4- recuperer les donn�es de sortie */
			String refPE = piEquivalente.getRef_piece();
			int idPE_cat = piEquivalente.getCategorie().getId_categorie();
			String refPO = piOrigine.getRef_piece_originale();
			String designPE = piEquivalente.getDesigniation();
			float pvPE = piEquivalente.getPvht();
			int stockPE = piEquivalente.getStock();
			int activePE = piEquivalente.getActive();

			retour = cStmt.getInt(8);
			ROWCOUNT = cStmt.getInt(9);

			Categorie categorie = new Categorie(idPE_cat);
			Piece pieceOrigine = new Piece(refPE, categorie, refPO, designPE, pvPE, stockPE, activePE);
			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- catcher les exceptions ajouter le try en 1 */
			if (retour == 0) {
				System.out.println("succes");
				return pieceOrigine;
			} // ok
			if (retour == 1062) {
				System.out.println("nom existant");
				return null;
			} // nom dupliqu�

			if (retour == 10000) {
				System.out.println("ExceptionSQL dans la procedure");
				return null;
			} // erreur SQL dans la procedure
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		return null;
	}

	@Override
	public ArrayList<Piece> getAll() throws BDDException {
		CallableStatement cStmt;
		ResultSet rs;
		ArrayList<Piece> alp = new ArrayList<Piece>();

		/* ************************ */

		/* ************************ */
		int retour, FOUNDROWS;

		try {

			/* 1- cree la variable qui doit executer la requette ou la PS */
			cStmt = cnx.prepareCall("CALL ps_select_PIECE (?, ?)");

			/* 2- donne les valeurs des parametres d'entrer */

			/* parametres dentrer */
			/*
			 * il n'ya pas de parametres d'entr�e puisque on select toute la
			 * table catgeorie
			 */

			/* parametres de sortie */
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);

			/* 3- execute requete */
			rs = cStmt.executeQuery();
			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(1);
			FOUNDROWS = cStmt.getInt(2);

			while (rs.next()) {
				String ref_piece = rs.getString("REF_PIECE");
				int id_categorie = rs.getInt("ID_CATEGORIE");
				String ref_piece_originale = rs.getString("REF_PIECE_ORIGINALE");
				String designiation = rs.getString("DESIGNIATION");
				int pvht = rs.getInt("PVHT");
				int stock = rs.getInt("STOCK");
				int active = rs.getInt("ACTIVE");

				Categorie categorie = new Categorie(id_categorie);
				Piece piece = new Piece(ref_piece, categorie, ref_piece_originale, designiation, pvht, stock, active);
				alp.add(piece);
			}

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return alp;
	}

	@Override
	public Piece getBy(String refPiece) throws BDDException {

		/* 0- Declaration de variable */
		CallableStatement cStmt;
		ResultSet rs;

		Categorie categorie;
		Piece piece = null;
		String ref_piece;
		int id_categorie;
		String libelle_catgeorie;
		String ref_piece_originale;
		String designiation;
		float pvht;
		int stock;
		int active;

		int retour, FOUNDROWS;
		/*
		 * Ordre des variable dans la ps REF_PIECE, p.ID_CATEGORIE,
		 * LIBELLE_CATEGORIE... ...REF_PIECE_ORIGINALE, DESIGNIATION, PVHT,
		 * STOCK, ACTIVE
		 */

		/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_sGetby_PIECE_by_REF_PIECE(?,?,?)");

			/* 2- donne les valeurs des parametres d'entrer */
			/* parametres � entrer */
			cStmt.setString(1, refPiece);

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			rs = cStmt.executeQuery();

			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(2);
			FOUNDROWS = cStmt.getInt(3);

			while (rs.next()) {
				ref_piece = rs.getString("REF_PIECE");
				id_categorie = rs.getInt("ID_CATEGORIE");
				libelle_catgeorie = rs.getString("LIBELLE_CATEGORIE");
				ref_piece_originale = rs.getString("REF_PIECE_ORIGINALE");
				designiation = rs.getString("DESIGNIATION");
				pvht = rs.getInt("PVHT");
				stock = rs.getInt("STOCK");
				active = rs.getInt("ACTIVE");

				categorie = new Categorie(id_categorie);
				piece = new Piece(ref_piece, categorie, ref_piece_originale, designiation, pvht, stock, active);
			}
			/* 5- fin de l'execution */
			cStmt.close();

		}
		/* 7- retourner (l'ArrayList, id...) */
		catch (SQLException e) {
			System.out.println("erreur SQL " + e.getMessage() + " -code- " + e.getErrorCode());
		}
		return piece;
	}

	public int associe(Piece piOrigine, Modele mo) throws BDDException {

		/* 0- Declaration de variable */
		CallableStatement cStmt;
		int retour = 0, FOUNDROWS = 0;

		/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_associe_insert_PIECE_MODELE(?,?,?,?)");
			/* 2- donne les valeurs des parametres */

			/* parametres � entrer */
			cStmt.setString(1, piOrigine.getRef_piece());
			cStmt.setString(2, mo.getRef_modele());
			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			cStmt.execute();

			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(3);
			FOUNDROWS = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom dupliqu�
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */

		return retour;

	}

	@Override
	public int update(Piece piece, float prix) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_update_PIECE(?,?,?,?);");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, piece.getRef_piece());
			cStmt.setFloat(2, prix);

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;
	}

	@Override
	public int delete(Piece piece) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_PIECE(?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, piece.getRef_piece());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/*
		 * 7- retourner la valeur de "retour" qui sera utiliser � peupler le
		 * statut du bean
		 */
		return retour;
	}

	public ArrayList<Piece> getOriginalesBy(Modele modele, Categorie categorie) throws BDDException {
		/* 0- Declaration de variable */
		CallableStatement cStmt;
		ResultSet rs;
		ArrayList<Piece> alP;
		alP = new ArrayList<Piece>();

		/* ************************ */
		int id_categorie;
		String libelle_categorie;
		String ref_modele;

		/* ************************ */
		Piece piece;
		String ref_piece;
		// int id_categorie;
		// String ref_modele;
		String ref_piece_originale;
		String designiation;
		float pvht;
		int stock;
		int active;
		piece = new Piece();
		/* ************************ */
		int retour, FOUNDROWS;

		try {

			/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
			cStmt = cnx.prepareCall("call ps_sGetby_PIECE_by_MODELE_CATEGORIE(?,?,?,?);");

			/* 2- donne les valeurs des parametres d'entrer */

			/* parametres � entrer */
			cStmt.setString(1, modele.getRef_modele());
			cStmt.setInt(2, categorie.getId_categorie());

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			rs = cStmt.executeQuery();
			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(3);
			FOUNDROWS = cStmt.getInt(4);

			while (rs.next()) {

				ref_piece = rs.getString("REF_PIECE");
				id_categorie = rs.getInt("ID_CATEGORIE");
				ref_piece_originale = rs.getString("REF_PIECE_ORIGINALE");
				designiation = rs.getString("DESIGNIATION");
				pvht = rs.getInt("PVHT");
				stock = rs.getInt("STOCK");
				active = rs.getInt("ACTIVE");

				piece = new Piece(ref_piece, categorie, ref_piece_originale, designiation, pvht, stock, active);
				alP.add(piece);
			}

			/* 5- fin de l'execution */
			cStmt.close();
			/* 6- catcher les exceptions ajouter le try en 1 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return alP;
	}

	@Override
	/* a revoir */ public ArrayList<Piece> getEquivalentesBy(Modele mo, Categorie ca) throws BDDException {
		// TODO Auto-generated method stub
		return null;
	}

}
