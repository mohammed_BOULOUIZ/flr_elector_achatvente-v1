package domain.dao.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.SQLException;

import java.util.ArrayList;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Marque;
import domain.Modele;
import domain.Type;
import domain.dao.ModeleDAO;

public class MySQLModeleDAO implements ModeleDAO {
	private Connection cnx;

	public MySQLModeleDAO(Connection cx) {
		cnx = cx;
	}

	public MySQLModeleDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Modele insert(String ref_modele, Type type, Marque marque) throws BDDException {

		/* Declaration */
		CallableStatement cStmt;
		Modele modele = null;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_insert_MODELE(?,?,?,?,?)");

			/* 2- donne les valeurs des parametres */

			/* parametres � entrer */
			cStmt.setString(1, ref_modele);
			cStmt.setInt(2, type.getId_type());
			cStmt.setInt(3, marque.getId_marque());

			/* parametres de sortie */
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(5, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			cStmt.execute();

			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(4);
			ROWCOUNT = cStmt.getInt(5);

			modele = new Modele(ref_modele, type, marque);
			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- catcher les exceptions ajouter le try en 1 */

		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());

		}
		if (retour == 0)
			return modele;
		else
			return null;

	}

	public ArrayList<Modele> getBy(Type type, Marque marque) throws BDDException {

		/* 0- Declaration de variable */
		CallableStatement cStmt;
		ResultSet rs;

		Modele modele;
		ArrayList<Modele> almodele;
		almodele = new ArrayList<Modele>();
		String ref_modele;
		int retour = 0, ROWCOUNT = 0;

		try {
			/* 1- cr�e la variable qui doit ex�cuter la requette ou la PS */
			cStmt = cnx.prepareCall("CALL ps_select_MODELE(?,?,?,?)");

			/* 2- donne les valeurs des parametres */
			cStmt.setInt(1, type.getId_type());
			cStmt.setInt(2, marque.getId_marque());

			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- ex�cute requete */
			rs = cStmt.executeQuery();
			while (rs.next()) {
				ref_modele = rs.getString("REF_MODELE");
				modele = new Modele(ref_modele, type, marque);
				almodele.add(modele);
			}
			/* 4- recuperer les donn�es de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- catcher les exceptions ajouter le try en 1 */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom dupliqu�
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return almodele;
	}

	@Override
	public int update_ref(String new_ref, Modele modele) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_update_MODELE (?,?,?,?);");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, new_ref);
			cStmt.setString(2, modele.getRef_modele());

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;
	}

	@Override
	public int delete(Modele modele) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_MODELE(?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, modele.getRef_modele());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/*
		 * 7- retourner la valeur de "retour" qui sera utiliser � peupler le
		 * statut du bean
		 */
		return retour;
	}

	public ArrayList<Modele> gatAllModele() throws BDDException {
		ArrayList<Modele> alMo = new ArrayList<Modele>();
		PreparedStatement pStmt;
		ResultSet rs;
		String ref_Modele = null;
		try {
			pStmt = cnx.prepareStatement("SELECT DISTINCT REF_MODELE FROM PIECE_MODELE");
			rs = pStmt.executeQuery();
			while (rs.next()) {
				ref_Modele = rs.getString("REF_MODELE");
				Modele modele = new Modele(ref_Modele);
				alMo.add(modele);
			}
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		return alMo;
	}

}
