package domain.dao.MySQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Categorie;

import domain.dao.CategorieDAO;

public class MySQLCategorieDAO implements CategorieDAO {

	private Connection cnx;

	public MySQLCategorieDAO(Connection cx) {
		cnx = cx;

	}

	public MySQLCategorieDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Categorie insert(Categorie ca) throws BDDException {
		/* Declaration */
		CallableStatement cStmt;
		Categorie cate = null;
		int retour;
		int ROWCOUNT;
		int id;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_insert_CATEGORIE (?,?,?,?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */

			cStmt.setString(1, ca.getLibelle_categorie());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			ROWCOUNT = cStmt.getInt(2);
			retour = cStmt.getInt(3);
			id = cStmt.getInt(4);

			cate = new Categorie(id, ca.getLibelle_categorie());
			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return cate;
	}

	public ArrayList<Categorie> getAll() throws BDDException {

		CallableStatement cStmt;
		ResultSet rs;
		ArrayList<Categorie> alC = new ArrayList<Categorie>();

		/* ************************ */
		int id_categorie;
		String libelle_categorie;

		/* ************************ */
		int retour, FOUNDROWS;

		try {

			/* 1- cree la variable qui doit executer la requette ou la PS */
			cStmt = cnx.prepareCall("CALL ps_select_CATEGORIE (?, ?)");

			/* 2- donne les valeurs des parametres d'entrer */

			/* parametres dentrer */
			/*
			 * il n'ya pas de parametres d'entr�e puisque on select toute la
			 * table catgeorie
			 */

			/* parametres de sortie */
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);

			/* 3- execute requete */
			rs = cStmt.executeQuery();
			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(1);
			FOUNDROWS = cStmt.getInt(2);

			while (rs.next()) {
				id_categorie = rs.getInt("ID_CATEGORIE");
				libelle_categorie = rs.getString("LIBELLE_CATEGORIE");

				Categorie ca = new Categorie(id_categorie, libelle_categorie);
				alC.add(ca);
			}

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return alC;
	}

	public int update(Categorie ca, String new_libelle) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_update_CATEGORIE(?,?,?,?);");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, ca.getLibelle_categorie());
			cStmt.setString(2, new_libelle);

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;
	}

	public int delete(Categorie ca) throws BDDException {

		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_CATEGORIE (?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, ca.getLibelle_categorie());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;
	}

}
