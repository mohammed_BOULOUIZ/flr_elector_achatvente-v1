package domain.dao.MySQL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import A_control.Main;
import domain.Type;
import domain.BDDConnexion;
import domain.BDDException;
import domain.dao.TypeDAO;

public class MySQLTypeDAO implements TypeDAO {
	private Connection cnx;
	private static final Logger logger = LogManager.getLogger(MySQLTypeDAO.class);

	/* Constructeur pour recuperer une connexion */

	public MySQLTypeDAO(Connection cx) {
		cnx = cx;
	}

	public MySQLTypeDAO() {
		try {
			cnx = BDDConnexion.getConnexion();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public Type insert(Type type) throws BDDException {
		/* Declaration */
		CallableStatement cStmt;

		int retour;
		int ROWCOUNT;
		int id;
		Type ty = null;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_insert_TYPE (?, ?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */

			cStmt.setString(1, type.getLibelle_type());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			ROWCOUNT = cStmt.getInt(2);
			retour = cStmt.getInt(3);
			id = cStmt.getInt(2);
			ty = new Type(id, type.getLibelle_type());
			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique		
				throw new BDDException(1062, "nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return ty;
	}

	@Override
	public ArrayList<Type> getAll() throws BDDException {
		CallableStatement cStmt;
		ResultSet rs;
		ArrayList<Type> alt = new ArrayList<Type>();

		/* ************************ */
		int id_type;
		String libelle_type;

		/* ************************ */
		int retour, FOUNDROWS;

		try {

			/* 1- cree la variable qui doit executer la requette ou la PS */
			cStmt = cnx.prepareCall("CALL ps_select_TYPE (?, ?)");

			/* 2- donne les valeurs des parametres d'entrer */

			/* parametres dentrer */
			/*
			 * il n'ya pas de parametres d'entr�e puisque on select toute la
			 * table catgeorie
			 */

			/* parametres de sortie */
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);

			/* 3- execute requete */
			rs = cStmt.executeQuery();
			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(1);
			FOUNDROWS = cStmt.getInt(2);

			while (rs.next()) {
				id_type = rs.getInt("ID_TYPE");
				libelle_type = rs.getString("LIBELLE_TYPE");

				Type ty = new Type(id_type, libelle_type);
				alt.add(ty);
			}

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
		} catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}

		/* 7- retourner (l'ArrayList, id...) */
		return alt;
	}

	@Override
	public int update(Type ty, String new_lib) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT = 0;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_update_TYPE (?,?,?,?);");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, ty.getLibelle_type());
			cStmt.setString(2, new_lib);

			/* parametres de sortie */
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(4, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(3);
			ROWCOUNT = cStmt.getInt(4);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1062) // nom duplique
				System.out.println("nom existant");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/* 7- retourner (l'ArrayList, id...) */
		return retour;

	}

	@Override
	public int delete(Type ty) throws BDDException {
		CallableStatement cStmt;
		int retour = 0;
		int ROWCOUNT;

		/* 1- cree la variable qui doit executer la requette ou la PS */
		try {
			cStmt = cnx.prepareCall("CALL ps_delete_TYPE(?, ?, ?)");

			/* 2- donne les valeurs des parametres */

			/* parametres e dentrer */
			cStmt.setString(1, ty.getLibelle_type());

			/* parametres de sortie */
			cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cStmt.registerOutParameter(3, java.sql.Types.INTEGER);

			/* 3- execute requete */
			cStmt.execute();

			/* 4- recuperer les donnees de sortie */
			retour = cStmt.getInt(2);
			ROWCOUNT = cStmt.getInt(3);

			/* 5- fin de l'execution */
			cStmt.close();

			/* 6- recuperer les retours afin de savoir si tous ses bien pass� */
			if (retour == 0) // ok
				System.out.println("succes");
			if (retour == 1451) // nom duplique
				System.out.println("nom est reference vers une autre table");
			if (retour == 10000) // erreur SQL dans la procedure
				System.out.println("ExceptionSQL dans la procedure");
		}

		catch (SQLException e) {
			throw new BDDException(e.getErrorCode(), e.getMessage());
		}
		/*
		 * 7- retourner la valeur de "retour" qui sera utiliser � peupler le
		 * statut du bean
		 */
		return retour;
	}

}
