package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Marque;
import domain.Modele;
import domain.Type;

public interface ModeleDAO {

	public abstract Modele insert(String ref_modele, Type type, Marque marque) throws BDDException;
	public abstract ArrayList<Modele> getBy(Type type, Marque marque) throws BDDException;
	public abstract int update_ref(String new_ref, Modele modele) throws BDDException;
	public abstract int delete(Modele modele) throws BDDException;
	
}
