package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Categorie;
import domain.Modele;
import domain.Piece;

public interface PieceDAO {

	public abstract Piece insertpO(Piece piOrigine) throws BDDException;
	public abstract ArrayList<Piece> getAll() throws BDDException;
	public abstract int update(Piece piece, float prix) throws BDDException;
	public abstract int delete(Piece piece) throws BDDException;

	public abstract Piece insertpE(Piece piEquivalente, Piece piOrigine) throws BDDException;
	public abstract int associe(Piece piOrigine, Modele mo) throws BDDException;
	
	public abstract Piece getBy(String refPiece) throws BDDException;
	public abstract ArrayList<Piece> getOriginalesBy(Modele mo, Categorie ca) throws BDDException;
	public abstract ArrayList<Piece> getEquivalentesBy(Modele mo, Categorie ca) throws BDDException;

}
