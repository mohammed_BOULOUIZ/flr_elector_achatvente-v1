package domain.dao;

import java.util.ArrayList;

import domain.BDDException;
import domain.Document;
import domain.Piece;

public interface DocumentDAO {

	public abstract Document insert (Document document, Piece piOrigine) throws BDDException;
	public abstract ArrayList<Document> getBy(Piece piOrigine) throws BDDException;
	public abstract int delete(Document d) throws BDDException;
}
