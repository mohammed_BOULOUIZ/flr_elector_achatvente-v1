package domain;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("06600a34-b029-41af-9eb1-1248d34144a3")
public class Marque {
	
	/* Attributs */
    @objid ("cd794556-1987-4478-9465-45ad04d80d7a")
    private int id_marque;
    @objid ("05b344bc-f4ef-4726-9583-db98d48701ce")
    private String libelle_marque;

    /* Construction */
    @objid ("ba97be78-4963-43db-a1b2-a7791329e956")
    public Marque() {
    }
	public Marque(int id_marque, String libelle_marque) {
		super();
		this.id_marque = id_marque;
		this.libelle_marque = libelle_marque;
	}
	public Marque(int id_marque) {
		super();
		this.id_marque = id_marque;
	}
	public Marque(String libelle_marque) {
		super();
		this.libelle_marque = libelle_marque;
	}

	
	/* Get_Set (!! Set Id_marque Interdit !!) */
	public String getLibelle_marque() {
		return libelle_marque;
	}

	public void setLibelle_marque(String libelle_marque) {
		this.libelle_marque = libelle_marque;
	}

	public int getId_marque() {
		return id_marque;
	}

	
}
