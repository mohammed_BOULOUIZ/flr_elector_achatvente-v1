package domain;
import java.util.ArrayList;
import java.util.List;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("a7fab87a-6423-4d73-8bc4-ac891faf9d22")
public class Modele {
	
	/* Attributs */
    @objid ("a0993571-0205-49db-8447-b8f9934ebc4d")
    private String ref_modele;
    @objid ("75f6b9b8-bb54-42ba-9fd5-ce1a746e5e10")
    private Type type;
    @objid ("f6162769-11ea-444d-b759-b675700894b6")
    private Marque marque;
    @objid ("1b4e63be-95d9-4794-a919-56733e4e0cb5")
    private List<Piece> piece = new ArrayList<Piece> ();

    /* Constructeur */
	public Modele(String ref_modele, Type type, Marque marque) {
		super();
		this.ref_modele = ref_modele;
		this.type = type;
		this.marque = marque;
	}
	public Modele(Type type, Marque marque) {
		super();
		this.type = type;
		this.marque = marque;
	}
	
	public Modele(String ref_modele) {
		super();
		this.ref_modele = ref_modele;
	}
	public Modele() {
		
	}
	
	/* Get_Set (!! Set Ref_modele Interdit !!) */
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Marque getMarque() {
		return marque;
	}
	public void setMarque(Marque marque) {
		this.marque = marque;
	}
	public List<Piece> getPiece() {
		return piece;
	}
	public void setPiece(List<Piece> piece) {
		this.piece = piece;
	}
	public String getRef_modele() {
		return ref_modele;
	}
	
		
}

