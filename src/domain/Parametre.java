package domain;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("c20c6180-031f-46c3-9799-3cee555ab68c")
public class Parametre {
	
	/* Attributs */
    @objid ("b2d92244-3e80-4825-bfae-396d4430061e")
    private String id;
    @objid ("c2f98e03-db2a-4b7a-b98e-9a1536703360")
    private String passeword;
    @objid ("e97f5c00-1d7f-4d4e-825a-7e00c82f2ec0")
    private String max_file_size;
    private String nom;
    private String prenom;
    

    /* Constructeur */
	public Parametre(String id, String passeword, String max_file_size) {
		super();
		this.id = id;
		this.passeword = passeword;
		this.max_file_size = max_file_size;
	}

	public Parametre(String id, String passeword) {
		super();
		this.id = id;
		this.passeword = passeword;
	}

	public Parametre(String id, String passeword, String nom, String prenom) {
		super();
		this.id = id;
		this.passeword = passeword;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public Parametre(String id, String passeword, String max_file_size, String nom, String prenom) {
		super();
		this.id = id;
		this.passeword = passeword;
		this.max_file_size = max_file_size;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Parametre() {
		super();
	}

	/* Get_Set */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPasseword() {
		return passeword;
	}

	public void setPasseword(String passeword) {
		this.passeword = passeword;
	}

	public String getMax_file_size() {
		return max_file_size;
	}

	public void setMax_file_size(String max_file_size) {
		this.max_file_size = max_file_size;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
