package domain;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("43830225-43b9-4993-82ba-bd53476dba9a")
public class Categorie {
	
	/* Attributs */
    @objid ("fdb8f1e3-a6be-4afe-8208-5d32bde7795b")
    private int id_categorie;
    @objid ("349c48b9-94ac-444d-8a8c-4f8a43f37f0c")
    private String libelle_categorie;

    /* Constructeur */
    public Categorie(int id_categorie, String libelle_categorie) {
		super();
		this.id_categorie = id_categorie;
		this.libelle_categorie = libelle_categorie;
	}
	public Categorie(String libelle_categorie) {
		super();
		this.libelle_categorie = libelle_categorie;
	}
	public Categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}	
	public Categorie() {
		super();
	}

	/* Get_Set (!! Set Id_categorie Interdit !!) */
	public String getLibelle_categorie() {
		return libelle_categorie;
	}
	public void setLibelle_categorie(String new_libelle_categorie) {
		this.libelle_categorie = new_libelle_categorie;
	}
	public int getId_categorie() {
		return id_categorie;
	}

}
