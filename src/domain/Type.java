package domain;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("7557fa10-e68f-45c9-9321-58155d79b060")
public class Type {
	
	/* Attributs */
    @objid ("32dfc6e8-8d73-4e2e-ba2b-7ca1461d0787")
    private int id_type;
    @objid ("2fc4aca3-aea2-41ef-b286-c65c72b6ac01")
    private String libelle_type;

    /* Constructeur */
	public Type(int id_type, String libelle_type) {
		super();
		this.id_type = id_type;
		this.libelle_type = libelle_type;
	}
	public Type(String libelle_type) {
		super();
		this.libelle_type = libelle_type;
	}
	public Type(int id_type) {
		super();
		this.id_type = id_type;
	}
	
	/* Get_Set (!! Set id_type interdit !!) */
	public String getLibelle_type() {
		return libelle_type;
	}
	public void setLibelle_type(String libelle_type) {
		this.libelle_type = libelle_type;
	}
	public int getId_type() {
		return id_type;
	}


}
