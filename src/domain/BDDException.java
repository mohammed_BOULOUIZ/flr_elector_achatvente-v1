package domain;

public class BDDException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4838805206376994118L;//pour que les autres classe s'y retrouve
	private int errorCode;

	/* Constructeur */
	public BDDException(int errorCode, String mes) {
		super(mes);
		this.errorCode = errorCode;
	}

	/* Get_code erreur*/ 
	public int getErrorCode() {
		return errorCode;
	}
	
}
