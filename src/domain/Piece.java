package domain;
import java.util.ArrayList;
import java.util.List;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("c32d349b-d343-4f35-9cc9-0e311e338bf5")
public class Piece {
	
	/* Attributs */
    @objid ("9ab9469f-0f5b-43ec-9e6c-631a72e295e5")
    private String ref_piece;
    @objid ("4425f97f-e1d4-4b59-8a5e-32f79bb08f61")
    private Categorie categorie;
    private String ref_piece_originale;
    @objid ("b24c6a3f-1efa-415f-a36b-3b7ece2f3482")
    private String designiation;
     @objid ("0d0abedb-6782-4300-a637-c353b8dab3f3")
    private float pvht;
    @objid ("a4abd1b6-e4aa-4f3d-83ff-ae2db720db0b")
    private int stock;
    @objid ("41fd6110-cbf1-4850-9b56-983cc73a5c67")
    private int active;
    @objid ("bcc86d49-a6d4-49bd-a63a-0bef89b33fc4")
    private List<Modele> modele = new ArrayList<Modele> ();

    @objid ("9371b959-8833-496c-82ca-318fee0fffbb")
    private List<Document> document = new ArrayList<Document> ();
    
    /* Constructeur */

	public Piece(String ref_piece, Categorie categorie, String ref_piece_originale, String designiation, float pvht,
			int stock, int active) {
		super();
		this.ref_piece = ref_piece;
		this.categorie = categorie;
		this.ref_piece_originale = ref_piece_originale;
		this.designiation = designiation;
		this.pvht = pvht;
		this.stock = stock;
		this.active = active;
	}

	

	public Piece(String ref_piece, Categorie categorie, String designiation, float pvht, int stock, int active,
			List<Modele> modele, List<Document> document) {
		super();
		this.ref_piece = ref_piece;
		this.categorie = categorie;
		this.designiation = designiation;
		this.pvht = pvht;
		this.stock = stock;
		this.active = active;
		this.modele = modele;
		this.document = document;
	}



	public Piece(String ref_piece) {
		super();
		this.ref_piece = ref_piece;
	}
	public Piece() {
		
	}
	
	public Piece(float pvht) {
		super();
		this.pvht = pvht;
	}
	/* Get_Set (!! Set Ref_piece Interdit !!)*/
	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	public String getRef_piece_originale() {
		return ref_piece_originale;
	}
	public void setRef_piece_originale(String ref_piece_originale) {
		this.ref_piece_originale = ref_piece_originale;
	}
	public String getDesigniation() {
		return designiation;
	}
	public void setDesigniation(String designiation) {
		this.designiation = designiation;
	}
	public float getPvht() {
		return pvht;
	}
	public void setPvht(float pvht) {
		this.pvht = pvht;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public List<Modele> getModele() {
		return modele;
	}
	public void setModele(List<Modele> modele) {
		this.modele = modele;
	}
	public List<Document> getDocument() {
		return document;
	}
	public void setDocument(List<Document> document) {
		this.document = document;
	}
	public String getRef_piece() {
		return ref_piece;
	}


	/* To String*/
	@Override
		public String toString() {
		return "Piece [ref_piece=" + ref_piece + ", categorie=" + categorie + ", ref_piece_originale="
				+ ref_piece_originale + ", designiation=" + designiation + ", pvht=" + pvht + ", stock=" + stock
				+ ", active=" + active + "]";
	}


}
