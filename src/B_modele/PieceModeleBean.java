package B_modele;

public class PieceModeleBean {

	private String ref_piece;
	private String ref_modele;
	private String status;
	
	public PieceModeleBean() {
		super();
	}

	public String getRef_piece() {
		return ref_piece;
	}

	public void setRef_piece(String ref_piece) {
		this.ref_piece = ref_piece;
	}

	public String getRef_modele() {
		return ref_modele;
	}

	public void setRef_modele(String ref_modele) {
		this.ref_modele = ref_modele;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
