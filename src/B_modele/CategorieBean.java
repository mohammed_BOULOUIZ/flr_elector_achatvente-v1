package B_modele;
import java.util.List;

import domain.Categorie;


public class CategorieBean {
	String libelle_categorie;
	int id_categorie;
	String statut;
	List<Categorie> lst;
	
	public CategorieBean() {
		super();
	}



	public String getLibelle_categorie() {
		return libelle_categorie;
	}



	public void setLibelle_categorie(String libelle_categorie) {
		this.libelle_categorie = libelle_categorie;
	}



	public int getId_categorie() {
		return id_categorie;
	}



	public void setId_categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}



	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public List<Categorie> getLst() {
		return lst;
	}

	public void setLst(List<Categorie> lst) {
		this.lst = lst;
	}
	

}
