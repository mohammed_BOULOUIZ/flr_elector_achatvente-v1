package B_modele;

import java.util.List;

import domain.Modele;



public class ModeleBean {
	private String libelle;
	private int id_type;
	private int id_marque;
	private String status;
	private List<Modele> lst;
	
	public ModeleBean() {
		super();
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Modele> getLst() {
		return lst;
	}

	public void setLst(List<Modele> lst) {
		this.lst = lst;
	}

	public int getId_type() {
		return id_type;
	}

	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	public int getId_marque() {
		return id_marque;
	}

	public void setId_marque(int id_marque) {
		this.id_marque = id_marque;
	}

	
	
}
