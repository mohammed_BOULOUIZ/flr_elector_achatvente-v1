package B_modele;

public class ParametreBean {
	/* Attributs */
    private String id;
    private String passeword;
    private String max_file_size;
    private String nom;
    private String prenom;
    private String statut;
	
    
    public ParametreBean() {
		super();
	}


	
    public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPasseword() {
		return passeword;
	}


	public void setPasseword(String passeword) {
		this.passeword = passeword;
	}


	public String getMax_file_size() {
		return max_file_size;
	}


	public void setMax_file_size(String max_file_size) {
		this.max_file_size = max_file_size;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getStatut() {
		return statut;
	}



	public void setStatut(String statut) {
		this.statut = statut;
	}
}
