package B_modele;

import java.util.List;

import domain.Type;

public class TypeBean {

	private String libelle;
	private int id;
	private String status;
	private List<Type> lst;
	
	public TypeBean() {
		super();
	}
	
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<domain.Type> getLst() {
		return lst;
	}
	public void setLst(List<Type> lsTy) {
		this.lst = lsTy;
	}


		
}

