package B_modele;

import java.util.List;

import domain.Categorie;
import domain.Piece;

public class PieceBean {
	private String ref_piece;
	private String designation;
	private Categorie categorie;
	private int id_categorie;
	private String ref_piece_originale;
	private float pvht;
	private int stock;
	private int active;
	private String status;
	private List<Piece> lst;
	private Piece piece;

	public PieceBean() {
		super();
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	public String getRef_piece() {
		return ref_piece;
	}

	public void setRef_piece(String ref_piece) {
		this.ref_piece = ref_piece;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Piece> getLst() {
		return lst;
	}

	public void setLst(List<Piece> lst) {
		this.lst = lst;
	}

	public int getId_categorie() {
		return id_categorie;
	}

	public void setId_categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}

	public String getRef_piece_originale() {
		return ref_piece_originale;
	}

	public void setRef_piece_originale(String ref_piece_originale) {
		this.ref_piece_originale = ref_piece_originale;
	}

	public float getPvht() {
		return pvht;
	}

	public void setPvht(float pvht) {
		this.pvht = pvht;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

}
