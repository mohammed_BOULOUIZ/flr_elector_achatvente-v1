package B_modele;

import java.util.List;

import domain.Marque;


public class MarqueBean {
	private String libelle;
	private int id;
	private String status;
	private List<Marque> lst;

	public MarqueBean() {
		super();
	}

	
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Marque> getLst() {
		return lst;
	}

	public void setLst(List<Marque> lsMrq) {
		this.lst = lsMrq;
	}

	
}
