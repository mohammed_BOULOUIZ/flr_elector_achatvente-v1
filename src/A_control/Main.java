package A_control;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import B_modele.CategorieBean;
import B_modele.MarqueBean;
import B_modele.ModeleBean;
import B_modele.ParametreBean;
import B_modele.PieceBean;
import B_modele.PieceModeleBean;
import B_modele.TypeBean;
import d_service.ApplicationService;
import d_service.DefaultApplicationService;
import domain.BDDConnexion;
import domain.BDDException;
import domain.Categorie;
import domain.Marque;
import domain.Modele;
import domain.Parametre;
import domain.Piece;
import domain.Type;

/**
 * Servlet implementation class main
 */
// @WebServlet(name="/Main")
public class Main extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private DefaultApplicationService dftAppService = new DefaultApplicationService();
	private static final Logger logger = LogManager.getLogger(Main.class);


	private void traiter(HttpServletRequest req, HttpServletResponse rep) throws ServletException, IOException {
		/* parametre login */
		String idt;
		String pswd;
		HttpSession session = req.getSession();

		idt = req.getParameter("id");
		pswd = req.getParameter("pswd");

		/* parametre de traitement */
		String vAction = req.getParameter("action");
		String adresse = null;

		String libelleEcrit = req
				.getParameter("libelleINS");/* le libelle � ajouter */
		String libellesupp = req
				.getParameter("libelleSUP");/* le libelle � supprimer */
		String libellechoisie = req
				.getParameter("libelleUP");/* le libelle � modifier */
		String new_libelle = req
				.getParameter("new_libelle");/* le nouveau libelle */
		String refrechercher = req
				.getParameter("refrechercher");/* le libelle � chercher */
		String idTyp = req.getParameter("id_type");
		String idMrq = req.getParameter("id_marque");
		String new_prix = req
				.getParameter("new_prix");/* le nouveau prix de la piece */
		String prixUP = req
				.getParameter("prixUP");/* l'ancien prix de la piece */
		String ref_modele = req.getParameter("ref_modele");
		String ref_piece = req.getParameter("ref_piece");
		String id_categorie = req.getParameter("id_categorie");
		String ref_piece_originale = req.getParameter("ref_piece_originale");
		String designation = req.getParameter("designation");
		String prix = req.getParameter("prix");
		String quantite = req.getParameter("quantite");
		String active = req.getParameter("active");

		List<Modele> lsMo = null;/* liste de Modeles */

		int retour = 0;

		/*******************************************************************************
		 *************************** GESTION DE LA TABLE TYPE****************************
		 *******************************************************************************/
		// si action est null

		if (vAction == null) {
			adresse = "/index.jsp";

		} else

		// si l'action vaut 'ajouterType'
		if (vAction.equals("ajouterType")) {
			adresse = "/Type_ajouter.jsp";

			if (libelleEcrit != null) {

				String libelletrim = libelleEcrit.trim();
				// instancier typeBean et le peupler
				TypeBean typeBeanaj = new TypeBean();
				typeBeanaj.setLibelle(libelletrim);
				req.setAttribute("lebeanajT", typeBeanaj);

				Type dftType = new Type(libelletrim);
				Type typ;
				try {
					 typ= dftAppService.insert(dftType);
					int id = typ.getId_type();

					if (id != -1) {
						typeBeanaj.setStatus("SUCCES");
					}
				} catch (BDDException e) {
					typeBeanaj.setStatus("ERREUR");
					logger.error(e.getErrorCode()+" "+e.getMessage());
				}

				req.setAttribute("lebeanajT", typeBeanaj);
			}
		} else

		// si l'action vaut 'listerTypes'
		if (vAction.equals("listerTypes")) {
			adresse = "/Type_RDU.jsp";
		} else

		// si l'action vaut 'supprimerType'
		if (vAction.equals("supprimerType")) {
			adresse = "/Type_RDU.jsp";

			if (libellesupp != null) {

				String libelleSUP_trim = libellesupp.trim();

				TypeBean typeBeansp = new TypeBean();
				typeBeansp.setLibelle(libelleSUP_trim);
				req.setAttribute("lebean_supp", typeBeansp);

				Type dftType = new Type(libelleSUP_trim);
				try {
					retour = dftAppService.delete(dftType);
				} catch (BDDException e) {
					logger.catching(e);
				}
				if (retour == 0)
					typeBeansp.setStatus("SUCCES");
				req.setAttribute("lebean_supp", typeBeansp);
				if (retour == 1451)
					typeBeansp.setStatus("ERREUR");
				req.setAttribute("lebean_supp", typeBeansp);
			}

		} else

		// si l'action vaut 'modififerType'
		if (vAction.equals("modifierType")) {
			adresse = "/Type_RDU.jsp";

			if (libellechoisie != null) {
				if (new_libelle != null) {

					String libelleUP_trim = libellechoisie.trim();
					String new_libelle_trim = new_libelle.trim();

					TypeBean typeBeanup = new TypeBean();
					typeBeanup.setLibelle(libelleUP_trim);
					req.setAttribute("lebean_up", typeBeanup);

					Type dftType = new Type(libelleUP_trim);

					try {
						retour = dftAppService.update(dftType, new_libelle_trim);
					} catch (BDDException e) {
						logger.catching(e);
					}
					if (retour == 0)
						typeBeanup.setStatus("SUCCES");
					req.setAttribute("lebean_up", typeBeanup);
					if (retour == 1062)
						typeBeanup.setStatus("ERREUR");
					req.setAttribute("lebean_up", typeBeanup);
				}
			}
		} else

		/*******************************************************************************
		 *************************** GESTION DE LA TABLE MARQUE*************************
		 *******************************************************************************/
		if (vAction.equals("ajouterMarque")) {
			adresse = "/Marque_ajouter.jsp";

			if (libelleEcrit != null) {

				String libelletrim = libelleEcrit.trim();
				// instancier typeBean et le peupler
				MarqueBean marqueBeanaj = new MarqueBean();
				marqueBeanaj.setLibelle(libelletrim);
				req.setAttribute("lebeanajM", marqueBeanaj);

				Marque marque = new Marque(libelletrim);
				try {
					Marque mrq = dftAppService.insert(marque);
					int id = mrq.getId_marque();

					if (id == -1) {
						marqueBeanaj.setStatus("ERREUR");
					} else {
						marqueBeanaj.setStatus("SUCCES");
					}
				} catch (BDDException e) {
					logger.catching(e);
				}

				req.setAttribute("lebeanajM", marqueBeanaj);
			}
		} else

		// si l'action vaut 'listerMarques'
		if (vAction.equals("listerMarques")) {
			adresse = "/Marque_RDU.jsp";

		} else
		// si l'action vaut 'supprimerMarque'
		if (vAction.equals("supprimerMarque")) {
			adresse = "/Marque_RDU.jsp";

			if (libellesupp != null) {

				String libelleSUP_trim = libellesupp.trim();

				MarqueBean marqueBeansp = new MarqueBean();
				marqueBeansp.setLibelle(libelleSUP_trim);
				req.setAttribute("lebean_supp", marqueBeansp);

				Marque dftMarque = new Marque(libelleSUP_trim);
				try {
					retour = dftAppService.delete(dftMarque);
				} catch (BDDException e) {
					logger.catching(e);
				}
				if (retour == 0)
					marqueBeansp.setStatus("SUCCES");
				req.setAttribute("lebean_supp", marqueBeansp);
				if (retour == 1451)
					marqueBeansp.setStatus("ERREUR");
				req.setAttribute("lebean_supp", marqueBeansp);
			}

		} else

		// si l'action vaut 'modififerMarque'
		if (vAction.equals("modifierMarque")) {
			adresse = "/Marque_RDU.jsp";

			if (libellechoisie != null) {
				if (new_libelle != null) {

					String libelleUP_trim = libellechoisie.trim();
					String new_libelle_trim = new_libelle.trim();

					MarqueBean marqueBeanup = new MarqueBean();
					marqueBeanup.setLibelle(libelleUP_trim);
					req.setAttribute("lebean_up", marqueBeanup);

					Marque dftMarque = new Marque(libelleUP_trim);

					try {
						retour = dftAppService.update(dftMarque, new_libelle_trim);
					} catch (BDDException e) {
						logger.catching(e);
					}
					if (retour == 0)
						marqueBeanup.setStatus("SUCCES");
					req.setAttribute("lebean_up", marqueBeanup);
					if (retour == 1062)
						marqueBeanup.setStatus("ERREUR");
					req.setAttribute("lebean_up", marqueBeanup);
				}
			}
		} else
		/*******************************************************************************
		 *************************** GESTION DE LA TABLE MODELE*************************
		 *******************************************************************************/

		// si l'action vaut 'listerModele'
		if (vAction.equals("listerModeles")) {
			adresse = "/Modele_RDU.jsp";

			if (idTyp != null && idMrq != null) {
				ModeleBean modeleBean = new ModeleBean();

				/* recuperer le ID de Type et le converitr en INT */
				int idTy = Integer.parseInt(idTyp);
				Type tyMo = new Type(idTy);

				/* recuperer le ID de Marque et le converitr en INT */
				int idMq = Integer.parseInt(idMrq);
				Marque mrqMo = new Marque(idMq);

				try {
					lsMo = dftAppService.getAllModele(tyMo, mrqMo);
				} catch (BDDException e) {
					logger.catching(e);
				}
				// instancier typeBean et le peupler
				modeleBean.setLst(lsMo);

				getServletContext().setAttribute("lebean_lsmo", modeleBean);

			}
		} else

		// si l'action vaut 'ajouterModele'
		if (vAction.equals("ajouterModele")) {
			adresse = "/Modele_ajouter.jsp";

			if (libelleEcrit != null && idTyp != null && idMrq != null) {

				/* recuperer le ID de Type et le converitr en INT */
				int idTy = Integer.parseInt(idTyp);
				Type tyMo = new Type(idTy);

				/* recuperer le ID de Marque et le converitr en INT */
				int idMq = Integer.parseInt(idMrq);
				Marque mrqMo = new Marque(idMq);

				/* recuperer le ecrit */
				String libelletrim = libelleEcrit.trim();

				// instancier typeBean et le peupler
				ModeleBean modeleBeanaj = new ModeleBean();

				modeleBeanaj.setLibelle(libelletrim);

				req.setAttribute("lebeanajMo", modeleBeanaj);

				Modele modele = new Modele();
				try {
					modele = dftAppService.insert(libelletrim, tyMo, mrqMo);
					if (modele != null) {
						modeleBeanaj.setStatus("SUCCES");
					} else if (modele == null) {
						modeleBeanaj.setStatus("ERREUR");
					}
				} catch (BDDException e) {
					logger.catching(e);

				}

				req.setAttribute("lebeanajMo", modeleBeanaj);
			}
		} else
		// si l'action vaut 'supprimerModele'
		if (vAction.equals("supprimerModele")) {
			adresse = "/Modele_RDU.jsp";
			if (libellesupp != null) {

				ModeleBean modeleBeansup = new ModeleBean();
				modeleBeansup.setLibelle(libellesupp);
				req.setAttribute("lebean_supp", modeleBeansup);

				Modele dftModele = new Modele(libellesupp);
				try {
					retour = dftAppService.delete(dftModele);
				} catch (BDDException e) {
					logger.catching(e);
				}
				if (retour == 0)
					modeleBeansup.setStatus("SUCCES");
				req.setAttribute("lebean_supp", modeleBeansup);
				if (retour == 1451)
					modeleBeansup.setStatus("ERREUR");
				req.setAttribute("lebean_supp", modeleBeansup);
			}
		} else
		// si l'action vaut 'modififerModele'
		if (vAction.equals("modifierModele")) {
			adresse = "/Modele_RDU.jsp";

			if (libellechoisie != null) {
				if (new_libelle != null) {

					/* recuperer les libelles */
					String libelleUP_trim = libellechoisie.trim();
					String new_libelle_trim = new_libelle.trim();

					Modele dftModele = new Modele(libelleUP_trim);

					ModeleBean modeleBeanup = new ModeleBean();
					modeleBeanup.setLibelle(libelleUP_trim);

					req.setAttribute("lebean_up", modeleBeanup);

					try {
						retour = dftAppService.update(dftModele, new_libelle_trim);
					} catch (BDDException e) {
						logger.catching(e);
					}
					if (retour == 0)
						modeleBeanup.setStatus("SUCCES");
					req.setAttribute("lebean_up", modeleBeanup);
					if (retour == 1062)
						modeleBeanup.setStatus("ERREUR");
					req.setAttribute("lebean_up", modeleBeanup);
				}
			}
		} else
		/*******************************************************************************
		 *************************** GESTION DE LA TABLE PIECE***************************
		 *******************************************************************************/
		// si l'action vaut 'listerPieces'
		if (vAction.equals("listerPieces")) {
			adresse = "/Piece_RDU.jsp";

		} else

		// si l'action vaut 'supprimerPiece'
		if (vAction.equals("supprimerPiece")) {
			adresse = "/Piece_RDU.jsp";

			if (libellesupp != null) {

				PieceBean pieceBeansp = new PieceBean();

				Piece piece = new Piece(libellesupp);
				try {
					retour = dftAppService.delete(piece);
				} catch (BDDException e) {
					logger.catching(e);
				}
				if (retour == 0)
					pieceBeansp.setStatus("SUCCES");
				req.setAttribute("lebean_supp", pieceBeansp);
				if (retour == 1451)
					pieceBeansp.setStatus("ERREUR");
				req.setAttribute("lebean_supp", pieceBeansp);
			}
		} else

		// si l'action vaut 'modifierPiece'
		if (vAction.equals("modifierPiece")) {
			adresse = "/Piece_RDU.jsp";
			if (libellechoisie != null) {
				if (new_prix != null) {
					float new_prix_num = Float.parseFloat(new_prix);
					Piece piece = new Piece(libellechoisie);

					PieceBean pieceBeanup = new PieceBean();

					try {
						dftAppService.update(piece, new_prix_num);
					} catch (BDDException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (retour == 0) {
						pieceBeanup.setStatus("SUCCES");
						req.setAttribute("lebean_up", pieceBeanup);
					} else {
						pieceBeanup.setStatus("ERREUR");
						req.setAttribute("lebean_up", pieceBeanup);
					}
				}
			}
		} else

		// si l'action vaut 'associerPM'
		if (vAction.equals("associerPM")) {

			adresse = "/Associer_PM.jsp";

			/*********** lister modele pour association *************/

			ModeleBean modeleBeanPM = new ModeleBean();
			DefaultApplicationService dftAppService_pm_mo = new DefaultApplicationService();
			try {
				lsMo = dftAppService_pm_mo.getAllModele();
			} catch (BDDException e) {
				logger.catching(e);
			}
			// instancier typeBean et le peupler
			modeleBeanPM.setLst(lsMo);
			getServletContext().setAttribute("lebean_lsmoass", modeleBeanPM);

			if (ref_piece != null && ref_modele != null) {
				adresse = "/Associer_PM.jsp";
				DefaultApplicationService dftAppService_pm = new DefaultApplicationService();
				PieceModeleBean pmBeanAss = new PieceModeleBean();
				String ref_piece_trim = ref_piece.trim();
				String ref_modele_trim = ref_modele.trim();

				Piece piece = new Piece(ref_piece_trim);
				Modele modele = new Modele(ref_modele_trim);

				try {
					retour = dftAppService_pm.associer(piece, modele);
				} catch (BDDException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (retour == 0) {
					pmBeanAss.setStatus("SUCCES");
					req.setAttribute("lebean_ass", pmBeanAss);
				} else {
					pmBeanAss.setStatus("ERREUR");
					req.setAttribute("lebean_ass", pmBeanAss);
				}
			}
		} else

		if (vAction.equals("ajouterPiece")) {
			adresse = "/Piece_ajouter.jsp";

			if (ref_piece != null && id_categorie != null && designation != null && prix != null && quantite != null) {

				PieceBean pieceBeanaj = new PieceBean();

				String ref_piece_trim = ref_piece.trim();
				int id_categorie_num = Integer.parseInt(id_categorie);
				String ref_piece_originale_trim = ref_piece_originale.trim();
				String designation_trim = designation.trim();
				float prix_num = Float.parseFloat(prix);
				int quantite_num = Integer.parseInt(quantite);
				int activ_num = Integer.parseInt(active);

				Categorie categorie = new Categorie(id_categorie_num);
				Piece piece = new Piece(ref_piece_trim, categorie, ref_piece_originale_trim, designation_trim, prix_num,
						quantite_num, activ_num);
				try {
					Piece pcs = dftAppService.insertO(piece);
					if (pcs != null) {
						pieceBeanaj.setStatus("SUCCES");
					} else {
						pieceBeanaj.setStatus("ERREUR");
					}
				} catch (BDDException e) {
					logger.catching(e);
				}
				req.setAttribute("lebeanajPc", pieceBeanaj);
			}

		} else
		/*******************************************************************************
		 *************************** GESTION DE LA TABLE CATEGORIE***************************
		 *******************************************************************************/
		/* si l'action veut listercategorie */
		if (vAction.equals("listerCategories")) {
			adresse = "/Categorie_RDU.jsp";

		} else

		/* si l'action veut ajoutercategorie */
		if (vAction.equals("ajouterCategorie")) {
			adresse = "/Categorie_ajouter.jsp";

			if (libelleEcrit != null) {

				String libelletrim = libelleEcrit.trim();
				// instancier CategorieBean et le peupler
				CategorieBean categorieBeanaj = new CategorieBean();
				categorieBeanaj.setLibelle_categorie(libelletrim);
				req.setAttribute("lebeanajC", categorieBeanaj);

				Categorie dftCategorie = new Categorie(libelletrim);
				try {
					Categorie cat = dftAppService.insert(dftCategorie);

					int id = cat.getId_categorie();
					if (id == -1) {
						categorieBeanaj.setStatut("ERREUR");
					} else {
						categorieBeanaj.setStatut("SUCCES");
					}
				} catch (BDDException e) {

					logger.catching(e);
				}

				req.setAttribute("lebeanajC", categorieBeanaj);
			}
		} else

		/* si l'action veut supprimercategorie */
		if (vAction.equals("supprimerCategorie")) {
			adresse = "/Categorie_RDU.jsp";

			if (libellesupp != null) {

				String libellesupptrim = libellesupp.trim();
				// instancier typeBean et le peupler
				CategorieBean categorieBeansup = new CategorieBean();
				categorieBeansup.setLibelle_categorie(libellesupptrim);
				req.setAttribute("lebeanajT", categorieBeansup);

				Categorie dftcategorie = new Categorie(libellesupptrim);
				try {
					retour = dftAppService.delete(dftcategorie);
					if (retour == 0) {
						categorieBeansup.setStatut("SUCCES");
						req.setAttribute("lebean_supp", categorieBeansup);
					}
					if (retour == 1451) {
						categorieBeansup.setStatut("ERREUR");
						req.setAttribute("lebean_supp", categorieBeansup);
					}

				} catch (BDDException e) {
					logger.catching(e);
				}

				req.setAttribute("lebean_supp", categorieBeansup);
			}
		} else

		/* si l'action veut modifiercategorie */
		if (vAction.equals("modifierCategorie")) {
			adresse = "/Categorie_RDU.jsp";
			if (libellechoisie != null) {
				if (new_libelle != null) {

					String libelleUP_trim = libellechoisie.trim();
					String new_libelle_trim = new_libelle.trim();

					CategorieBean categorieBeanup = new CategorieBean();
					categorieBeanup.setLibelle_categorie(libelleUP_trim);
					req.setAttribute("lebean_up", categorieBeanup);

					Categorie dftCategorie = new Categorie(libelleUP_trim);

					try {
						retour = dftAppService.update(dftCategorie, new_libelle_trim);
					} catch (BDDException e) {
						logger.catching(e);
					}
					if (retour == 0) {
						categorieBeanup.setStatut("SUCCES");
						req.setAttribute("lebean_up", categorieBeanup);
					}
					if (retour == 1062) {
						categorieBeanup.setStatut("ERREUR");
						req.setAttribute("lebean_up", categorieBeanup);
					}
				}
			}
		} else

		/*******************************************************************************
		 ********************************************************************************
		 *************************** NAVIGATION CLIENT***********************************
		 *******************************************************************************/
		/* si l'action veut lister les pieces en cascade */
		if (vAction.equals("cascademodele")) {
			adresse = "/index.jsp";
			if (idTyp != null && idMrq != null) {

				ModeleBean modeleBeanCascade = new ModeleBean();

				/* recuperer le ID de Type et le converitr en INT */
				int idTy = Integer.parseInt(idTyp);
				Type tyMo = new Type(idTy);

				/* recuperer le ID de Marque et le converitr en INT */
				int idMq = Integer.parseInt(idMrq);
				Marque mrqMo = new Marque(idMq);

				try {
					lsMo = dftAppService.getAllModele(tyMo, mrqMo);
				} catch (BDDException e) {
					logger.catching(e);
				}
				// instancier typeBean et le peupler
				modeleBeanCascade.setLst(lsMo);

				req.setAttribute("lebean_lsmocas", modeleBeanCascade);
			}
		} else
		// /*si l'action veut trouver une piece*/
		if (vAction.equals("listerpiececascade")) {
			adresse = "/1_ResultCascade_pieces.jsp";
			if (ref_modele != null && id_categorie != null) {
				int id_cat = Integer.parseInt(id_categorie);
				Categorie categorie = new Categorie(id_cat);
				Modele modele = new Modele(ref_modele);
				PieceBean pieceBeanCascade = new PieceBean();
				List<Piece> lsp = null;
				try {
					lsp = dftAppService.getPieceby_CatMo(modele, categorie);
				} catch (BDDException e) {
					logger.catching(e);
				}

				pieceBeanCascade.setLst(lsp);
				req.setAttribute("lebean_lspccas", pieceBeanCascade);
			}

		} else if (vAction.equals("piecebyref")) {
			adresse = "/1_ResultTrouve_piece.jsp";
			if (refrechercher != null) {
				String ref = refrechercher.trim();
				PieceBean pieceBean = new PieceBean();
				Piece piece = new Piece();
				piece = null;
				try {
					piece = dftAppService.getPieceBy(ref);

				} catch (BDDException e) {
					logger.catching(e);
				}
				if (piece == null) {
					pieceBean.setStatus("ERREUR");
				}
				if (piece != null) {
					pieceBean.setStatus("SUCCES");

					pieceBean.setRef_piece(piece.getRef_piece());
					pieceBean.setId_categorie(piece.getCategorie().getId_categorie());
					pieceBean.setRef_piece_originale(piece.getRef_piece_originale());
					pieceBean.setDesignation(piece.getDesigniation());
					pieceBean.setPvht(piece.getPvht());
					pieceBean.setStock(piece.getActive());
					pieceBean.setActive(piece.getActive());
				}
				req.setAttribute("beanPiece_ref", pieceBean);
			}
		} else

		if (vAction.equals("connecte")) {
			adresse = "/index.jsp";
			if (idt != null && pswd != null) {
				String id = idt.trim();
				ParametreBean paramBean = new ParametreBean();
				Parametre param = new Parametre(id, pswd);
				Parametre paramConnexion = new Parametre();

				try {
					paramConnexion = dftAppService.connexion(param);

				}

				catch (BDDException e) {
					logger.catching(e);
				}
				if (paramConnexion == null) {
					paramBean.setStatut(null);
				}
				if (paramConnexion != null) {
					paramBean.setStatut("SUCCES");
					paramBean.setId(paramConnexion.getId());
					paramBean.setPasseword(paramConnexion.getPasseword());
					paramBean.setNom(paramConnexion.getNom());
					paramBean.setPrenom(paramConnexion.getPrenom());
				}
				session.setAttribute("beanParam", paramBean);
			}
		} else if (vAction.equals("deconnecte")) {
			adresse = "/index.jsp";
			session.invalidate();
		}

		this.getServletContext().getRequestDispatcher(adresse).forward(req, rep);
	}

	public void init() {
		List<Type> lsTy = null;/* liste de Types */
		List<Marque> lsMrq = null;/* liste de Marques */
		List<Piece> lsPc = null;/* liste de Pieces */
		List<Categorie> lstcat = null;/* liste de categorie */
		DefaultApplicationService dftAppService = new DefaultApplicationService();
		/*********************** Liste des types *************************/
		TypeBean typeBean = new TypeBean();
		try {

			lsTy = dftAppService.getAllTypes();

		} catch (BDDException e) {
			logger.catching(e);
		}
		// instancier typeBean et le peupler
		typeBean.setLst(lsTy);
		getServletContext().setAttribute("lebean_lsty", typeBean);

		/*********************** Liste des marques *************************/
		MarqueBean marqueBean = new MarqueBean();
		try {
			lsMrq = dftAppService.getAllMarque();
		} catch (BDDException e) {
			logger.catching(e);
		}
		// instancier typeBean et le peupler
		marqueBean.setLst(lsMrq);
		getServletContext().setAttribute("lebean_lsmq", marqueBean);

		/*********************** Liste les pieces *************************/
		PieceBean piecebean = new PieceBean();
		try {
			lsPc = dftAppService.getAllPiece();
		} catch (BDDException e) {
			logger.catching(e);
		}
		piecebean.setLst(lsPc);
		getServletContext().setAttribute("lebean_lspc", piecebean);

		/*********************** Liste les categories *************************/
		CategorieBean categorieBean = new CategorieBean();
		DefaultApplicationService dftAppService_PC_CAT = new DefaultApplicationService();
		try {
			lstcat = dftAppService_PC_CAT.getAllCategorie();
		} catch (BDDException e1) {
			System.out.println(e1.getMessage() + e1.getErrorCode());
		}
		categorieBean.setLst(lstcat);
		getServletContext().setAttribute("lebean_lscat", categorieBean);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Main() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		dftAppService.closeBDD();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		traiter(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		traiter(request, response);
	}

}