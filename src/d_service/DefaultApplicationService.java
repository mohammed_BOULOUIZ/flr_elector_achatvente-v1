package d_service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.BDDConnexion;
import domain.BDDException;
import domain.Categorie;
import domain.Marque;
import domain.Modele;
import domain.Parametre;
import domain.Piece;
import domain.Type;
import domain.dao.MySQL.MySQLCategorieDAO;
import domain.dao.MySQL.MySQLMarqueDAO;
import domain.dao.MySQL.MySQLModeleDAO;
import domain.dao.MySQL.MySQLPieceDAO;
import domain.dao.MySQL.MySQLTypeDAO;
import domain.dao.MySQL.MySQLUtilisateurDAO;

public class DefaultApplicationService implements ApplicationService {

	private MySQLPieceDAO pieceDAO;
	private MySQLModeleDAO modeleDAO;
	private MySQLMarqueDAO marqueDAO;
	private MySQLTypeDAO typeDAO;
	private MySQLCategorieDAO categorieDAO;
	private MySQLUtilisateurDAO parametreDAO;
	private Connection cnx;

	public DefaultApplicationService() {
		super();
		cnx = BDDConnexion.getConnexion();
		if (cnx!=null){
			pieceDAO = new MySQLPieceDAO(cnx);
			modeleDAO = new MySQLModeleDAO(cnx);
			marqueDAO = new MySQLMarqueDAO(cnx);
			typeDAO = new MySQLTypeDAO(cnx);
			categorieDAO = new MySQLCategorieDAO(cnx);
			parametreDAO = new MySQLUtilisateurDAO(cnx);
		}else
			System.out.println("la cnx esy null dans dftappli");
	}

	@Override
	public void closeBDD() {
		try {
			cnx.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage() + e.getErrorCode() + "et donc" + e.getCause());
		}
	}

	/********************** DAO TYPE ***********************************/
	@Override
	public ArrayList<Type> getAllTypes() throws BDDException {
		ArrayList<Type> lsType;
		lsType = typeDAO.getAll();
		return lsType;
	}

	@Override
	public Type insert(Type ty) throws BDDException {
		Type typ = typeDAO.insert(ty);
		return typ;
	}

	@Override
	public int update(Type ty, String newL) throws BDDException {
		int retour = typeDAO.update(ty, newL);
		return retour;
	}

	@Override
	public int delete(Type ty) throws BDDException {
		int retour = typeDAO.delete(ty);
		return retour;
	}

	@Override
	public Type getTypeBy(String libelle) throws BDDException {

		return null;
	}

	@Override
	public Type getTypeBy(int id) throws BDDException {
		// TODO Auto-generated method stub
		return null;
	}

	/********************** DAO Marque ***********************************/

	/* mettre � jour l'interface Application service */
	public ArrayList<Marque> getAllMarque() throws BDDException {
		ArrayList<Marque> lsMrq;
		lsMrq = marqueDAO.getAll();
		return lsMrq;
	}

	public Marque insert(Marque mq) throws BDDException {
		Marque mrq = marqueDAO.insert(mq);
		return mrq;
	}

	public int delete(Marque mq) throws BDDException {
		int retour = marqueDAO.delete(mq);
		return retour;
	}

	public int update(Marque mq, String newL) throws BDDException {
		int retour = marqueDAO.update(mq, newL);
		return retour;
	}

	/********************** DAO MODELE ***********************************/

	public List<Modele> getAllModele(Type type, Marque marque) throws BDDException {
		ArrayList<Modele> lsMod;
		lsMod = modeleDAO.getBy(type, marque);
		return lsMod;
	}

	public List<Modele> getAllModele() throws BDDException {
		ArrayList<Modele> lsAllMod;
		lsAllMod = modeleDAO.gatAllModele();
		return lsAllMod;
	}

	public Modele insert(String ref_modele, Type type, Marque marque) throws BDDException {
		Modele modele = new Modele();
		modele = modeleDAO.insert(ref_modele, type, marque);
		return modele;
	}

	public int delete(Modele modele) throws BDDException {
		int retour = modeleDAO.delete(modele);
		return retour;
	}

	public int update(Modele modele, String new_libelle_trim) throws BDDException {
		int retour = modeleDAO.update_ref(new_libelle_trim, modele);
		return retour;
	}

	/********************** DAO PIECE ***********************************/
	public List<Piece> getAllPiece() throws BDDException {
		List<Piece> lsPc = pieceDAO.getAll();
		return lsPc;
	}

	public Piece getPieceBy(String libelle) throws BDDException {
		Piece piece = pieceDAO.getBy(libelle);
		return piece;
	}

	public Piece insertO(Piece piOrigine) throws BDDException {
		Piece piece = pieceDAO.insertpO(piOrigine);
		return piece;
	}

	public Piece insertE(Piece piEquivalente, Piece piOrigine) throws BDDException {
		Piece piece = pieceDAO.insertpE(piEquivalente, piOrigine);
		return piece;
	}

	public int associer(Piece piece, Modele modele) throws BDDException {
		int retour = pieceDAO.associe(piece, modele);
		return retour;
	}

	public int delete(Piece piece) throws BDDException {
		int retour = pieceDAO.delete(piece);
		return retour;
	}

	public int update(Piece piece, float prix) throws BDDException {
		int retour = pieceDAO.update(piece, prix);
		return retour;
	}
	
	public ArrayList<Piece> getPieceby_CatMo(Modele modele, Categorie categorie) throws BDDException{
		ArrayList<Piece> alp=new ArrayList<Piece>();
		alp=pieceDAO.getOriginalesBy(modele, categorie);
		return alp;
	}

	/********************** DAO CATEGORIE ***********************************/
	public List<Categorie> getAllCategorie() throws BDDException {
		List<Categorie> lsCat = categorieDAO.getAll();
		return lsCat;
	}

	public Categorie insert(Categorie categorie) throws BDDException {
		Categorie cat = categorieDAO.insert(categorie);
		return cat;
	}

	public int delete(Categorie categorie) throws BDDException {
		int retour = categorieDAO.delete(categorie);
		return retour;
	}

	public int update(Categorie categorie, String new_libelle) throws BDDException {
		int retour = categorieDAO.update(categorie, new_libelle);
		return retour;
	}

	/********************** DAO Connexion ***********************************/
	public Parametre connexion(Parametre param) throws BDDException{
		Parametre parametre = parametreDAO.getUser(param);
		return parametre;
	}


}
