package d_service;

import java.util.ArrayList;
import domain.BDDException;
import domain.Type;

public interface ApplicationService {
	public	abstract	 ArrayList<Type> getAllTypes() throws BDDException;
	public	abstract	 Type insert(Type ty) throws BDDException;
	public	abstract	 int update(Type ty, String newL) throws BDDException;
	public	abstract	 int delete(Type ty) throws BDDException;
	public	abstract	 Type getTypeBy(String libelle)	throws BDDException;
	public	abstract	 Type getTypeBy(int	id)	throws BDDException;
	public	abstract	 void closeBDD();
}
